<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ElectricityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', function () {
    return view('index');
});

/*******************
*@function:login page(admin and user)
*******************/ 
Route::get("login",[ElectricityController::class,'check']);
/*******************
*@function:loginpage view
*******************/ 
Route::get('log_in', function () {
    return view('login');
});
/*******************
*@function:rgister page view
*******************/

Route::get("users",[ElectricityController::class,'register']);

Route::get('admindashboard', function () {
    return view('admindashboard');
});

Route::view('/userdashboard','userdashboard');


/*******************
*@function:rgister page view
*******************/

//Route::get("users",[ElectricityController::class,'register']);

/*******************
*@function:registered user page  view by admin
*******************/ 
 
Route::get("tableview",[ElectricityController::class, 'dataview']);

Route::get("logout",[ElectricityController::class,'logout']);

/*******************
*@function:viewing single passenger record 
*******************/

Route::get("getrecord",[ElectricityController::class, 'recordview']);

Route::get("getview",[ElectricityController::class, 'singleview']);

Route::get('/records/{id}','ElectricityController@singlerecord');

Route::get("/billReciept",[ElectricityController::class,'index']);
Route::get('/getAmount/{id}', [ElectricityController::class,'getAmount']);
/*******************
*@function:viewing preview
*******************/
Route::get("preview",[ElectricityController::class,'preview']);

Route::get("billcalc",[ElectricityController::class,'billcalc']);

/*******************
*@function:bill page view
*******************/

Route::get('bil',[ElectricityController::class,'bill']);

/*******************
*@function:registered user page  view by admin
*******************/ 
 
Route::get("bilview",[ElectricityController::class, 'dataviews']);
/*******************
*@function:bilpage view
*******************/ 
Route::get('bil_in', function () {
    return view('bil');
});

//Route::get('/show/{id}','ElectricityController@display')->consumer_no('display');
